<?php

class Frog extends Animal{

  public $jump = "hop hop";

  public function jump(){
    echo $this->jump;
  }

}

class Ape extends Animal{
  
  public $yell = "Auooo";

  public function yell(){
    echo $this->yell;
  }

}