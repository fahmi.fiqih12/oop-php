<?php

class Animal{

  public $name,
         $legs,
         $cold_bloded;

  public function __construct($name,$legs=2,$cold_bloded="false"){
    $this->name = $name;
    $this->legs = $legs;
    $this->cold_bloded = $cold_bloded;
  }

}